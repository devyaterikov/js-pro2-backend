const jwt = require('jsonwebtoken')

function checkAuth(req, res, next) {
    const token = req.headers['authorization']
    if (token == ''){
        return res.status('401').send('Не авторизован')
    }
    jwt.verify(token, 'secret', function(err, decoded){
        if (err) return res.status('406').send('Ошибка!');
        req.decoded = decoded;
        next();
    })
}

module.exports = {checkAuth};