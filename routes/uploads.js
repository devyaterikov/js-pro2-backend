var express = require('express');
var router = express.Router();
const Users = require('../models/users')
const jwt = require('jsonwebtoken')
const multer = require('multer')
const upload = multer({dest: 'uploads/'})

router.put('/', upload.single('image'), (req, res) => {
    res.send(req.file.path)
})

module.exports = router;