const news = require('./news');
const chats = require('./chats');
const store = require('./store');
const login = require('./login');
const users = require('./users');
const uploads = require('./uploads');

function route(app){
  app.use('/news', news);
  app.use('/chats', chats);
  app.use('/store', store);
  app.use('/login', login);
  app.use('/users', users);
  app.use('/uploads', uploads);
}

module.exports = route;
