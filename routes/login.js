var express = require('express');
var router = express.Router();
const Users = require('../models/users')
const jwt = require('jsonwebtoken')

router.post('/', (req, res) => {
    let {username, password} = req.body
    Users.findOne({username}).then(user => {
        if (user.check_password(password)){
            let token = jwt.sign(
                {
                    data: {username: user.username}
                },
                "secret",
                {expiresIn: "10h"}
            )
            res.send(token)
        }
        else{
            throw new Error('Error!')
        }
    })
    .catch(err => {res.status('404').send(err)})
})

module.exports = router;