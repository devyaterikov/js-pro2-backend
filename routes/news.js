var express = require('express');
var router = express.Router();
const News = require('../models/news');
const fs = require('fs');
const {checkAuth} = require("../middleware/auth");

router.get('/', (req, res) => {
    News.find().sort({ $natural: -1 }).then(news => {res.send(news)})
})
router.put('/', checkAuth, (req, res) => {
    let news = new News(req.body)
    news.save()
    res.send('ok')
})
router.post('/', checkAuth, (req, res) => {
    News.update({_id: req.body.news._id}, {$set: req.body.news})
    .then(() => {res.send('ok')})
    .catch(err => {res.send('Error!')})
})
router.delete('/:id', checkAuth, (req, res) => {
    News.findOne({_id: req.params.id}).select('img').then(nw => {
        if (nw.img != '') fs.unlinkSync(nw.img)
    })
    News.deleteOne({_id: req.params.id}).then(() => {res.send('ok')}).catch(err => {res.send('Error!')})
})

module.exports = router;