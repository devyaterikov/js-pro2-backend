var express = require('express');
var router = express.Router();
const Store = require('../models/store')
const {checkAuth} = require("../middleware/auth");

router.get('/', (req, res) => {
    Store.find().then(store => {res.send(store)})
})
router.put('/', checkAuth, (req, res) => {
    let store = new Store(req.body)
    store.save()
    res.send(store)
})
router.post('/', checkAuth, (req, res) => {
    Store.update({_id: req.body.product._id}, {$set: req.body.product}).then(res.send('ok'))
})
router.delete('/:id', checkAuth, (req, res) => {
    Store.deleteOne({_id: req.params.id}).then(res.send('ok'))
})

module.exports = router;