const mongoose = require('mongoose');
const username = process.env.DB_USERNAME;
const password = process.env.DB_PASSWORD;
mongoose.connect(`mongodb+srv://${username}:${password}@jspro-wwjh0.gcp.mongodb.net/test`, {useNewUrlParser: true});