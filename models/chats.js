const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var chatsSchema = new Schema({
    title: String,
    img: String,
    admin: String,
    users: [String],
    messages: [{
        user: String,
        text: String
    }]
});
const Chats = mongoose.model("Chats", chatsSchema);
module.exports = Chats