const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var newsSchema = new Schema({
    title: String,
    text: String,
    img: String,
    like: Number,
    comment: Number,
    share: Number,
    user: String
});
const News = mongoose.model("News", newsSchema);
module.exports = News